def qt_ui_library(name, ui, deps):
  """Compiles a QT UI file and makes a library for it.

  Args:
    name: A name for the rule.
    src: The ui file to compile.
    deps: cc_library dependencies for the library.
  """
  native.genrule(
      name = "%s_uic" % name,
      srcs = [ui],
      outs = ["ui_%s.hh" % ui.split('.')[0]],
      cmd = "uic $(locations %s) -o $@" % ui,
  )

  native.cc_library(
      name = name,
      hdrs = [":%s_uic" % name],
      deps = deps,
  )

def qt_cc_library(name, sources, headers, normal_srcs=[], normal_hdrs=[], deps=None, ui=None,
                  ui_deps=None, **kwargs):
  """Compiles a QT library and generates the MOC for it.

  If a UI file is provided, then it is also compiled with UIC.

  Args:
    name: A name for the rule.
    src: The cpp file to compile.
    hdr: The single header file that the MOC compiles to src.
    normal_hdrs: Headers which are not sources for generated code.
    deps: cc_library dependencies for the library.
    ui: If provided, a UI file to compile with UIC.
    ui_deps: Dependencies for the UI file.
    kwargs: Any additional arguments are passed to the cc_library rule.
  """
  hdr_names = []
  for hdr in headers:
    hdr_name = hdr.split(".")[0]
    hdr_names.append(hdr_name)
    
    native.genrule(
      name = "%s_moc" % hdr_name,
      srcs = [hdr],
      outs = ["moc_%s.cc" % hdr_name],
      cmd = "moc $(location %s) -o $@ -f'$(location %s)'" \
      % (hdr, hdr),
    )
    
  srcs = [":%s_moc" % hdr for hdr in hdr_names] + sources + normal_srcs
  if ui:
    qt_ui_library("%s_ui" % name, ui, deps=ui_deps)
    deps.append("%s_ui" % name)

  hdrs = headers + normal_hdrs

  native.cc_library(
      name = name,
      srcs = srcs,
      hdrs = hdrs,
      deps = deps,
      **kwargs
  )
