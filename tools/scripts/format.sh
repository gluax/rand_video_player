#!/usr/bin/env bash
cd "$BUILD_WORKING_DIRECTORY"

folders=("fileio/" "frontend/" "third_party/")

for folder in "${folders[@]}"; do
    find "${folder}" -regex '.*\.\(cpp\|hpp\|cu\|c\|h\)' -exec clang-format -style=file -i {} \;
done;
