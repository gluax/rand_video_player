#!/usr/bin/env bash
cd "$BUILD_WORKING_DIRECTORY"

find . -type f -name "*~" -exec rm {} \;
