ENV_VARS = select({
    "@bazel_tools//src/conditions:darwin": [
        "QT_HOME",
    ],
    "//conditions:default": [
        "QT_HOME",
        "X11_HOME",
    ],
    "@bazel_tools//src/conditions:windows": [
        "QT_HOME",
        "QT_LIB",
        "WINDOWS_SDKS",
        "WINDOWS_SDKS_LIBS",
    ],    
})

COPTS = select({
	"@bazel_tools//src/conditions:darwin": [
		"-std=c++17",
		"-stdlib=libc++",
		"-F/Library/Frameworks",
		"-Wall",
		"-Werror",
		"-pedantic",
		"-O3",
	],
    "//conditions:default": [
		"-std=c++17",
		"-Wall",
		"-Werror",
		"-pedantic",
		"-O3",
	],
	"@bazel_tools//src/conditions:windows": [
		"/std:c++17",
        #"/W4",
		#"/O2",
	],
})


LINKOPTS = select({
    "@bazel_tools//src/conditions:darwin": [
    ],
    "//conditions:default": [
    ],
    "@bazel_tools//src/conditions:windows": [
    ],
})

QT_CORE_DEPS = select({
     "//conditions:default": [
        "@qt//:qt_core",
    ],
    "@bazel_tools//src/conditions:windows": [
        "@qt//:qt_core_lib_import",
    ]
})

QHOTKEYMAP_DEPS = select({
    "@bazel_tools//src/conditions:darwin": [
        "@qt//:qt_network",
        "@qt//:qt_qml",
    ],
    "//conditions:default": [
        "@qt//:qt_network",
        "@qt//:qt_qml",
    ],
    "@bazel_tools//src/conditions:windows": [
        "@qt//:qt_network_lib_import",
        "@qt//:qt_qml_lib_import",
    ]
}) + QT_CORE_DEPS

QHOTKEY_DEPS = select({
    "@bazel_tools//src/conditions:darwin": [
        "-framework Carbon",
        "@qt//:qt_gui",
    ],
    "//conditions:default": [
        "@qt//:qt_gui",
        "@qt//:qt_x11",
    ],
    "@bazel_tools//src/conditions:windows": [
        "@qt//:qt_gui_lib_import",
        "@win//:win_sdk_user_lib_import",
    ]
}) + QT_CORE_DEPS

QHOTKEY_SRCS = select({
    "@bazel_tools//src/conditions:darwin": [
        "qhotkey_mac.cc",
    ],
    "//conditions:default": [
        "qhotkey_x11.cc",
    ],
    "@bazel_tools//src/conditions:windows": [
        "qhotkey_win.cc",
    ],
})

FRONTEND_DEPS = select({
    "@bazel_tools//src/conditions:darwin": [
        "@qt//:qt_quick",
        "@qt//:qt_quickcontrols2",
        "@qt//:qt_widgets",
    ],
    "//conditions:default": [
        "@qt//:qt_quick",
        "@qt//:qt_quickcontrols2",
        "@qt//:qt_widgets",
    ],
    "@bazel_tools//src/conditions:windows": [
        "@qt//:qt_quick_lib_import",
        "@qt//:qt_quickcontrols2_lib_import",
        "@qt//:qt_widgets_lib_import",
    ]
})
