#include <QtCore/QDebug>
#include <QtGui/QIcon>
#include <QtGui/QKeySequence>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include <QtQuickControls2/QQuickStyle>
#include <QtWidgets/QApplication>

#include "fileio/fileio.hh"
#include "hotkey_map/hotkey_map.hh"
#include "third_party/qhotkey/qhotkey.hh"

int main(int argc, char *argv[]) {
  QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

  QApplication app(argc, argv);
  // app.setWindowIcon(QIcon("icons/wife.png"));
  app.setOrganizationDomain("temp");
  app.setApplicationName("Rand Video Player");
  QQuickStyle::setStyle("Material");

  QQmlApplicationEngine engine;
  engine.rootContext()->setContextProperty(
      "appDirPath", QApplication::applicationDirPath());

  engine.setOfflineStoragePath(QApplication::applicationDirPath());

  FileIO fileIO;
  engine.rootContext()->setContextProperty("fileio", &fileIO);

  HotkeyMap hotkeyMap;
  engine.rootContext()->setContextProperty("hotkeyMap", &hotkeyMap);

  qmlRegisterType<QHotkey>("QHotkey", 1, 0, "QHotkey");
  engine.load(QUrl(QStringLiteral("frontend/views/main.qml")));

  qDebug() << engine.offlineStoragePath();

  return app.exec();
}
