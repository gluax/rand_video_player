import Qt.labs.settings 1.1

Settings {
    property bool autoplay_video;
    property int autoplay_delay;
    property bool transitions;
    property bool randomize_videos;
    property bool randomize_transitions;
    property bool remove_played_videos;
    property bool video_status;
    property var videos;
    property bool shortcuts_enabled;
    // property var transitions;
    property int video_index;
    property bool video_or_transition;
    property string lastFolderString;
    property url lastFolderURL;
    property var recentProjects;
    property string openProject;
    property string openProjectPath;
}
