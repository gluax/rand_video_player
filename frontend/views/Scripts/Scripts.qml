import QtQuick 2.9
import QtQuick.Controls 2.15
import QtQuick.Dialogs 1.1

Item {
    property var callbacks: {
	"VideoPlay": () => video_player_page.video_play()
    }

    property var global_hotkey_text: {
	"dummy": "dne"
    }

    function delay(time, cb) {
	const timer = Qt.createQmlObject("import QtQuick 2.0; Timer {}", window);
	timer.interval = time;
	timer.repeat = false;
	timer.triggered.connect(cb);
	timer.start();
    }
    
    function closeProject() {
	if (settings.openProject === '') {
	    openError("No Open Project", () => {});
	    return;
	}
	
	settings.openProject = '';
	settings.openProjectPath = '';
	settings.videos = [];
	settings.video_status = false;
	settings.video_index = 0;
	settings.video_or_transition = true;
	video_player_page.set_source('');
    }
    
    function createInstance(component) {
	let instance;

	if (component.status === Component.Ready) {
	    instance = component.createObject(window);
	} else if (component.status === Component.Error) {
	    console.log("Error loading component:", component.errorString());
	    Qt.quit();
	} else {
	    component.statusChanged.connect(() => {
		if (component.status === Component.Ready) {
		    instance = component.createObject(window);
		} else if (component.status === Component.Error) {
		    console.log("Error loading component:", component.errorString());
		    Qt.quit();
		}
	    });
	}

	return instance;
    }

    function createComponentInstance(path) {
	const C = Qt.createComponent(`../${path}`);
	const I = createInstance(C);
	
	return I;
    }

    function createGlobalHotkey(keys, modifiers, hotkey_text, id, action) {
	if (hotKeyBound(hotkey_text)) return false;
	if (hotKeyIdBound(id, keys, modifiers, hotkey_text)) return true;
	const HK = createComponentInstance('Hotkey/Hotkey.qml');
	
	HK.setShortcut(keys, modifiers, true);
	HK.onActivated.connect(action);
	
	registerHotKey(id, hotkey_text, HK);

	return true;
    }

    function createHotkeyInput() {
	const HKI = createComponent('Pages/Components/HotkeyInput/HotkeyInput.qml');
	
    }

    function globalHotkeyCallbackLookup(id) {
	const cb = this.callbacks[id];
	
	return cb ? cb : () => console.error("error id dne");
    }

    function globalHotkeyTextLookup(id) {
	const text = this.global_hotkey_text[id];
	
	return text ? text : "";
    }

    function hotkeysList() {
	const keys = hotkeyMap.hotkey_text_keys();

	let hotkey_details = [];
	for (let i = 0; i < keys.length; i++) {
	    if (!hotkeyMap.exists_hotkey_text(keys[i])) continue;
	    const id = hotkeyMap.get_hotkey_text(keys[i]);
	    
	    hotkey_details.push({
		"text": keys[i],
		"id": id,
		"global": id.includes("Global"),
	    });
	}

	return hotkey_details;
    }
    
    function hotKeyBound(hotkey_text) {
	if (hotkeyMap.exists_hotkey_text(hotkey_text)) {
	    openError(
		`Key Already Bound By ${hotkeyMap.get_hotkey_text(hotkey_text)}.`,
		() => {}
	    );

	    return true;
	}

	return false;
    }

    function hotKeyIdBound(id, keys, modifiers, hotkey_text) {
	if (hotkeyMap.exists_hotkey_object(id)) {
	    const HK = hotkeyMap.get_hotkey_object(id);
	    
	    const old_hotkey_key_code = HK.keyCode();
	    const old_hotkey_modifiers = HK.modifiers()
	    const old_hotkey_text = validateHotKey(old_hotkey_modifiers, old_hotkey_key_code);
	    if (!old_hotkey_text) return false;

	    HK.setShortcut(keys, modifiers, true);
	    
	    hotkeyMap.remove_hotkey_text(old_hotkey_text, id);
	    hotkeyMap.insert_hotkey_text(hotkey_text, id);
	    
	    return true;
	}

	return false;
    }

    function loadGlobalHotkey(text, id) {
	const non_global_id = id.substring(6, id.length);
	const cb = globalHotkeyCallbackLookup(non_global_id);
	const keys_text = text.split('+');
	const { keys, modifiers } = validateHotkeyText(keys_text);
	createGlobalHotkey(keys, modifiers, text, id, cb);
	
	this.global_hotkey_text[id] = text;
	console.log(id, text, this.global_hotkey_text[id]);
    }

    function loadHotkey(text, id) {
	console.log("TODO: lhk", text, id);
    }

    function loadHotkeyList(hotkeys) {
	for (let i = 0; i < hotkeys.length; i++) {
	    const { global, text, id } = hotkeys[i];
	    global ? loadGlobalHotkey(text, id) : loadHotkey(text, id);
	}
    }

    function openError(error_text, abort, retry) {
	const ED = createComponentInstance('Error/Error.qml');

	ED.text = error_text;
	ED.standardButtons = retry ? StandardButton.Abort | StandardButton.Retry : StandardButton.Abort;

	ED.onRejected.connect(() => {
	    if (!ED) return;
	    ED.clickedButton == StandardButton.Abort ?
		abort()
		: {}; ED.destroy();
	});
	
	retry ?
	    ED.onAccepted.connect(() => {
		if (!ED) return;
		ED.clickedButton == StandardButton.Retry ?
		    retry()
		    : {};
		ED.destroy();
	    })
	    : {};
	
	ED.open();
    }

    function openProject(dialog) {
	const split = Qt.platform.os === "windows" ? '///' : '//';
	const filePath = dialog.fileUrl.toString().split(split)[1];
	
	const validProject = fileio.folderExists(filePath + "/videos");
	updateLastFolder(filePath.substring(filePath.lastIndexOf('/') + 1));
	
	const projects = JSON.parse(settings.recentProjects || '[]');
	
	if (validProject) {
	    updateRecentProjects(filePath);
	    if (!settings.videos) settings.videos = [];
	    settings.videos = fileio.scanFolderFiles(filePath + "/videos");
	    video_player_page.load();
	} else {
	    return;
	}

	const transitions_dir = fileio.folderExists(filePath + "/transitions");
	if (transitions_dir) {
	    settings.videostransitions = fileio.scanFolderFiles(filePath + "/transitions");
	}
    }

    function openVideos(filePath) {
	const videos_path = filePath + "/videos";
	const videos_dir = fileio.folderExists(videos_path);
	
	if (videos_dir) {
	    settings.videos = fileio.scanFolderFiles(videos_path);
	    video_player_page.load();
	} else {
	    return;
	}

	const transitions_path = filePath + "/transitions";
	const transitions_dir = fileio.folderExists(transitions_path);
	if (transitions_dir) {
	    settings.transitions = fileio.scanFolderFiles(transitions_path);
	}
    }

    function registerHotKey(id, hotkey_text, object) {
	hotkeyMap.insert_hotkey_object(id, object);
	hotkeyMap.insert_hotkey_text(hotkey_text, id);
    }

    function save() {
	smk2.saveSettings();
	console.log('TODO feed vars to save project');
	// smk2.saveProject();
    }

    function quit() {
	save();
	Qt.quit();
    }
    
    function updateLastFolder(folder) {
	settings.lastFolderString = folder;
	settings.lastFolderURL = folder;
	settings.sync();
    }

    function updateRecentProjects(filePath) {
	let projects = JSON.parse(settings.recentProjects || '[]');
	const projectIndex = projects.map((project) => { return project.filePath; }).indexOf(filePath);

	if (projectIndex >= 0) {
	    projects.splice(projectIndex, 1);
	}
	
	const project = filePath.split('/').splice(-1)[0];
	settings.openProjectPath = filePath;
	settings.openProject = project;
	projects = [{ filePath: filePath, project: project }].concat(projects);

	if (projects.length > 5) {
	    projects.pop();
	}

	settings.recentProjects = JSON.stringify(projects);
	settings.sync();
    }

    function validateHotkeyText(keys_text) {
	let modifiers = [];
	let keys = [];

	for (let i = 0; i < keys_text.length; i++) {
	    const key_text = keys_text[i];
	    
	    switch(key_text) {
	    case "Ctrl":
		modifiers.push(Qt.ControlModifier);
		break;
	    case "Alt":
		modifiers.push(Qt.AltModifier);
		break;
	    case "Shift":
		modifiers.push(Qt.ShiftModifier);
		break;
	    case "Meta":
		modifiers.push(Qt.MetaModifier);
		break;
	    case "A":
		keys.push(Qt.Key_A);
		break;
	    case "B":
		keys.push(Qt.Key_B);
		break;
	    case "C":
		keys.push(Qt.Key_C);
		break;
	    case "D":
		keys.push("D");
		break;
	    case "E":
		keys.push("E");
		break;
	    case "F":
		keys.push(Qt.Key_F);
		break;
	    case "G":
		keys.push(Qt.Key_G);
		break;
	    case "H":
		keys.push(Qt.Key_H);
		break;
	    case "I":
		keys.push(Qt.Key_I);
		break;
	    case "J":
		keys.push(Qt.Key_J);
		break;
	    case "K":
		keys.push(Qt.Key_K);
		break;
	    case "L":
		keys.push(Qt.Key_L);
		break;
	    case "M":
		keys.push(Qt.Key_M);
		break;
	    case "N":
		keys.push(Qt.Key_N);
		break;
	    case "O":
		keys.push(Qt.Key_O);
		break;
	    case "P":
		keys.push(Qt.Key_P);
		break;
	    case "Q":
		keys.push(Qt.Key_Q);
		break;
	    case "R":
		keys.push(Qt.Key_R);
		break;
	    case "S":
		keys.push(Qt.Key_S);
		break;
	    case "T":
		keys.push(Qt.Key_T);
		break;
	    case "U":
		keys.push(Qt.Key_U);
		break;
	    case "V":
		keys.push(Qt.Key_V);
		break;
	    case "W":
		keys.push(Qt.Key_W);
		break;
	    case "X":
		keys.push(Qt.Key_X);
		break;
	    case "Y":
		keys.push(Qt.Key_Y);
		break;
	    case "Z":
		keys.push(Qt.Key_Z);
		break;
	    default:
		scripts.openError(
		    "Unkown Key Press",
		    () => {
			hotkey_play_video_item.focus = false;
			settings.shortcuts_enabled = true;
		    },
		    () => {
			console.log("recall this function passing event info to it");
		    },
		);
		return;
	    }
	}

	return {
	    keys: keys.reduce((total, element) => total | element),
	    modifiers: modifiers.reduce((total, element) => total | element),
	};
    }

    function validateHotKey(modifiers, key_code) {
	let keys = [];
	
	if (modifiers & Qt.ControlModifier) keys.push("Ctrl");
	if (modifiers & Qt.AltModifier) keys.push("Alt");
	if (modifiers & Qt.ShiftModifier) keys.push("Shift");
	if (modifiers & Qt.MetaModifier) keys.push("Meta");
	if (keys.length === 0) return false;

	switch(key_code) {
	case Qt.Key_A:
	    keys.push("A");
	    break;
	case Qt.Key_B:
	    keys.push("B");
	    break;
	case Qt.Key_C:
	    keys.push("C");
	    break;
	case Qt.Key_D:
	    keys.push("D");
	    break;
	case Qt.Key_E:
	    keys.push("E");
	    break;
	case Qt.Key_F:
	    keys.push("F");
	    break;
	case Qt.Key_G:
	    keys.push("G");
	    break;
	case Qt.Key_H:
	    keys.push("H");
	    break;
	case Qt.Key_I:
	    keys.push("I");
	    break;
	case Qt.Key_J:
	    keys.push("J");
	    break;
	case Qt.Key_K:
	    keys.push("K");
	    break;
	case Qt.Key_L:
	    keys.push("L");
	    break;
	case Qt.Key_M:
	    keys.push("M");
	    break;
	case Qt.Key_N:
	    keys.push("N");
	    break;
	case Qt.Key_O:
	    keys.push("O");
	    break;
	case Qt.Key_P:
	    keys.push("P");
	    break;
	case Qt.Key_Q:
	    keys.push("Q");
	    break;
	case Qt.Key_R:
	    keys.push("R");
	    break;
	case Qt.Key_S:
	    keys.push("S");
	    break;
	case Qt.Key_T:
	    keys.push("T");
	    break;
	case Qt.Key_U:
	    keys.push("U");
	    break;
	case Qt.Key_V:
	    keys.push("V");
	    break;
	case Qt.Key_W:
	    keys.push("W");
	    break;
	case Qt.Key_X:
	    keys.push("X");
	    break;
	case Qt.Key_Y:
	    keys.push("Y");
	    break;
	case Qt.Key_Z:
	    keys.push("Z");
	    break;
	default:
	    scripts.openError(
		"Unkown Key Press",
		() => {
		    hotkey_play_video_item.focus = false;
		    settings.shortcuts_enabled = true;
		},
		() => {
		    console.log("recall this function passing event info to it");
		},
	    );
	    return;
	}

	return keys.join("+");
    }
}
