import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3

Menu {
    id: tools_menu
    title: "Tools"
    width: 300

    function openPage(page) {
	page_view.currentItem === page ?
	    page_view.replace(video_player_page)
	    : page_view.replace(page);
    }

    MenuItem {
	property string hotkey_id: "Settings"
	
	action: Action {
	    enabled: settings.shortcuts_enabled
	    text: "Settings"
	    // icon.source: "../../icons/hammer.png"
	    icon.color: Material.iconColor
	    shortcut: "Ctrl+S"
	    onTriggered: openPage(options_page);
	}

	text: "Ctrl+S"
	contentItem: Menuitem {}
    }

    MenuItem {
	property string hotkey_id: "ConfigureVideos"
	
	action: Action {
	    enabled: settings.shortcuts_enabled
	    text: "Configure Videos"
	    icon.source: "../../icons/hammer.png"
	    icon.color: Material.iconColor
	    shortcut: "Ctrl+V"
	    onTriggered: openPage(config_videos_page);
	}

	text: "Ctrl+V"
	contentItem: Menuitem {}
    }

    MenuItem {
	property string hotkey_id: "ConfigureProjects"
	
	action: Action {
	    enabled: settings.shortcuts_enabled
	    text: "Configure Projects"
	    icon.source: "../../icons/hammer.png"
	    icon.color: Material.iconColor
	    shortcut: "Ctrl+P"
	    onTriggered: openPage(config_projects_page);
	}

	text: "Ctrl+P"
	contentItem: Menuitem {}
    }
}
