import QtQuick 2.9
import QtQuick.Controls 2.5

MenuBar {
    FileMenu {}
    WindowMenu {}
    ToolsMenu {}
    HelpMenu {}
}
