import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3

Menu {
    id: help_menu
    title: "Help"
    width: 300

    MenuItem {
	property string hotkey_id: "About"
	
	action: Action {
	    text: "About"
	    // icon.source: "../../icons/hammer.png"
	    icon.color: Material.iconColor
	    shortcut: "Ctrl+A"
	    onTriggered: {
		console.log('About description')
	    }
	}

	text: "Ctrl+A"
	contentItem: Menuitem {}
    }

    MenuItem {
	property string hotkey_id: "Help"
	
	action: Action {
	    text: "Help"
	    // icon.source: "../../icons/hammer.png"
	    icon.color: Material.iconColor
	    shortcut: "Ctrl+H"
	    onTriggered: {
		console.log('help description')
	    }
	}

	text: "Ctrl+H"
	contentItem: Menuitem {}
    }
}
