import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtQuick.LocalStorage 2.12

Menu {
    id: file_menu
    title: "File"
    width: 300

    FileDialog {
	id: openProject
	title: "Select a project folder."
	selectFolder: true
	selectMultiple: false

	Component.onCompleted: {
	    openProject.folder = settings.lastFolderURL | Qt.platform.os === "windows" ? 'file:///C:/' : 'file:///';
	}

	onAccepted: scripts.openProject(this)
    }
    
    MenuItem {
	property string hotkey_id: "LoadProject"
	
	action: Action {
	    enabled: settings.shortcuts_enabled
	    text: "Load Project..."
	    icon.source: "../../icons/folder-open-outline.png"
	    icon.color: Material.iconColor
	    shortcut: "Ctrl+O"

	    onTriggered: {
		openProject.open();
	    }
	}

	text: "Ctrl+O"
	contentItem: Menuitem {}
    }
    
    Menu {
	id: recentProjectsMenu
	cascade: true
	title: "Recent Projects"
	
	onOpenedChanged: {
	    while (recentProjectsMenu.takeItem(0)) {
		recentProjectsMenu.removeItem(0);
	    }
	    
	    const projects = JSON.parse(settings.recentProjects || '[]');
	    
	    if (projects.length === 0)  {
		const item = Qt.createQmlObject(`import QtQuick 2.13; import QtQuick.Controls 2.13; MenuItem {}`, recentProjectsMenu);
		item.text = `No recent projects.`;
		recentProjectsMenu.addItem(item);
		return;
	    }
	    
	    for (let index in projects) {
		const item = Qt.createQmlObject(`import QtQuick 2.13; import QtQuick.Controls 2.13; MenuItem {onTriggered: { var fp = '${projects[index].filePath}'; scripts.updateRecentProjects(fp); scripts.openVideos(fp); } }`, recentProjectsMenu);
		item.text = `${~~index+1}: ${projects[index].project}`;
		recentProjectsMenu.addItem(item);
	    }
	}
    }
    
    MenuSeparator {}

    MenuItem {
	action: Action {
	    enabled: settings.shortcuts_enabled
	    text: "Close Project"
	    icon.source: "../../icons/close-outline.png"
	    icon.color: Material.iconColor

	    onTriggered: scripts.closeProject();
	}

	text: ''
	contentItem: Menuitem {}
    }

    MenuSeparator {}

    MenuItem {
	property string hotkey_id: "Quit"
	
	action: Action {
	    enabled: settings.shortcuts_enabled
	    id: quitAction
	    text: "Exit"
	    icon.source: "../../icons/exit-run.png"
	    icon.color: Material.iconColor
	    shortcut: "Ctrl+Q"

	    onTriggered: scripts.quit()
	}

	text: "Ctrl+Q"
	contentItem: Menuitem {}
    }
}
