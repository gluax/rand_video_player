import QtQuick 2.9
import QtQuick.LocalStorage 2.15

Item {
    property var db;
    
    property bool autoplay_video;
    property int autoplay_delay;

    property bool transitions;

    property bool randomize_videos;
    property bool randomize_transitions;

    property bool remove_played_videos;
    property bool remove_played_transitions;

    property var videos_list;
    property var transitions_list;

    property bool shortcuts_enabled;

    property int video_index;
    property int transitions_index;

    property bool video_or_transition;

    property string last_folder_string;
    property url last_folder_url;
    property var recent_projects;
    property string open_project;
    property string open_project_path;

    property int default_volume;

    property var projects;
    
    function createDBConnection() {
	const db = LocalStorage.openDatabaseSync(
	    "RandVidPlayer",
	    "1.0",
	    "Setting storage for application.",
	    1000000
	)

	db.transaction(
	    (tx) => {
		tx.executeSql("CREATE TABLE IF NOT EXISTS Settings(id TEXT NOT NULL PRIMARY KEY, autoplay_video INTEGER NOT NULL, autoplay_delay INTEGER NOT NULL, transitions INTEGER NOT NULL, randomize_videos INTEGER NOT NULL, randomize_transitions INTEGER NOT NULL, remove_played_videos INTEGER NOT NULL, remove_played_transitions INTEGER NOT NULL, last_folder_string TEXT, last_folder_url TEXT, open_project TEXT, open_project_path TEXT, default_volume INTEGER NOT NULL, hotkey_bindings TEXT NOT NULL)");

		const settings_results = tx.executeSql("SELECT * FROM Settings");

		if (settings_results.rows.length === 0) {
		    tx.executeSql(
			"INSERT INTO Settings VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
			[
			    "settings",
			    false,
			    0,
			    false,
			    false,
			    false,
			    false,
			    false,
			    "",
			    "",
			    "",
			    "",
			    50,
			    "[]"
			]
		    )
		}
		
		tx.executeSql("CREATE TABLE IF NOT EXISTS Projects(project_name TEXT PRIMARY KEY NOT NULL, path TEXT NOT NULL, video_confs TEXT, project_hotkey TEXT)");
	    }
	);
	
	return db
    }

    function loadSettings() {
	this.db.readTransaction(
	    (tx) => {
		const results = tx.executeSql("SELECT * FROM Settings");

		console.log('rrl', results.rows.length);

		this.videos_list = [];
		this.transitions_list = [];
		this.shortcuts_enabled = true;
		this.video_index = 0;
		this.transitions_index = 0;
		this.recent_projects = [];
		this.video_or_transition = true;
		
		this.autoplay_video = results.rows.item(0).autoplay_video;
		this.autoplay_delay = results.rows.item(0).autoplay_delay;
		this.transitions = results.rows.item(0).transitions;
		this.randomize_videos = results.rows.item(0).randomize_videos;
		this.randomize_transitions = results.rows.item(0).randomize_transitions;
		this.remove_played_videos = results.rows.item(0).remove_played_videos;
		this.remove_played_transitions = results.rows.item(0).remove_played_transitions;
		this.last_folder_string = results.rows.item(0).last_folder_string || "";
		this.last_folder_url = results.rows.item(0).last_folder_url || "";
		this.open_project = results.rows.item(0).open_project;
		this.open_project_path = results.rows.item(0).open_project_path;
		this.default_volume = results.rows.item(0).default_volume;

		const hotkey_bindings = results.rows.item(0).hotkey_bindings;
		console.log("hkd", hotkey_bindings); // TODO
		// need to have a store of global call back options
		const hotkeys = JSON.parse(hotkey_bindings || "[]");
		scripts.loadHotkeyList(hotkeys);
	    }
	)
    }

    function saveSettings() {
	try {
	    const hotkey_bindings = JSON.stringify(scripts.hotkeysList());
	    
	    this.db.transaction(
		(tx) => {
		    tx.executeSql(
			`UPDATE Settings 
SET autoplay_video = ?, autoplay_delay = ?, transitions = ?, 
randomize_videos = ?, randomize_transitions = ?, 
remove_played_videos = ?, remove_played_transitions = ?, 
last_folder_string = ?, last_folder_url = ?, 
open_project = ?, open_project_path = ?, 
default_volume = ?, hotkey_bindings = ? 
WHERE id = "settings"`,
			[
			    this.autoplay_video,
			    this.autoplay_delay,
			    this.transitions,
			    this.randomize_videos,
			    this.randomize_transitions,
			    this.remove_played_videos,
			    this.remove_played_transitions,
			    this.last_folder_string,
			    this.last_folder_url,
			    this.open_project,
			    this.open_project_path,
			    this.default_volume,
			    hotkey_bindings,
			]
		    )
		}
	    )
	} catch (err) {
	    console.log("error inserting data", err);
	}
    }

    function insertProject(project_name, project_path, video_confs, project_hotkey) {
	this.db.transaction(
	    (tx) => {
		tx.executeSql(
		    "INSERT INTO Projects VALUES(?, ?, ?, ?)",
		    [
			project_name,
			project_path,
			JSON.stringify(video_confs),
			JSON.stringify(project_hotkey)
		    ]
		)
	    }
	)
    }

    function saveProject(project_name, video_confs, project_hotkey) {	
	this.db.transaction(
	    (tx) => {
		tx.executeSql(
		    `UPDATE Projects
SET video_confs = ?, project_hotkey = ?, 
WHERE project_name = ?`
		    [
			project_name,
			JSON.stringify(video_confs),
			JSON.stringify(project_hotkey)
		    ]
		)
	    }
	)
    }

    function loadProjects() {
	this.db.readTransaction(
	    (tx) => {
		const results = tx.executeSql("SELECT * FROM Projects");
		this.projects = results.rows;
	    }
	)
    }
    
    Component.onCompleted: {
	this.db = createDBConnection();
	loadSettings();
	loadProjects();
    }
}
