import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.3

import "Smk2"
import "AppSettings"
import "Menubar"
import "Pages/VideoPlayer"
import "Pages/Options"
import "Pages/ConfigProjects"
import "Pages/ConfigVideos"
import "Scripts"

ApplicationWindow {
    id: window
    property bool windowed: true
    visible: true
    width: 1400
    height: 800
    title: "Random Video Player"

    Material.theme: Material.Dark
    Material.accent: Material.DeepPurple

    onClosing: scripts.save()

    Component.onCompleted: {
	
	class Test {
	    constructor(hello) {
		this.hello = hello;
	    }
	}
	
	settings.shortcuts_enabled = true;

	setX(Screen.width / 2 - width / 2);
	setY(Screen.height / 2 - height / 2);
	
	Object.defineProperty(Array.prototype, 'shuffle', {
	    value: function() {
		for (let i = this.length - 1; i > 0; i--) {
		    const j = Math.floor(Math.random() * (i + 1));
		    [this[i], this[j]] = [this[j], this[i]];
		}
		
		return this;
	    }
	});
	
	if (settings.openProject === '') scripts.openVideos(settings.openProjectPath);
	video_player_page.load();
    }

    Smk2 {
	id: smk2
    }
    
    AppSettings {
	id: settings
    }

    ConfigProjects {
	id: config_projects_page
    }

    ConfigVideos {
	id: config_videos_page
    }

    menuBar: MenuBar {
	id: menu_bar
    }

    Options {
	id: options_page
    }

    StackView {
	id: page_view
	anchors.fill: parent
	focus: true
	initialItem: video_player_page
    }

    Scripts {
	id: scripts
    }

    VideoPlayer {
	id: video_player_page
    }
}
