import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Page {
    function render(projects) {
	for (let i = 0; i < projects.length; i++) {
	    const project_name = projects.item(i).project_name;
	    const path = projects.item(i).path;
	    const video_confs = JSON.parse(projects.item(i).video_confs || "{}");
	    const project_hotkey = JSON.parse(projects.item(i).project_hotkey || "{}");

	    if (!Object.keys(project_hotkey) === 0) console.log("TODO load hotkey");
	    
	}
    }
    
    Component.onCompleted: {
	scripts.delay(500, () => {
	    console.log('TODO load each project hot key if it exists');
	});
    }
    
    Text {
	text: "Todo for each known project render project name and HotkeyInput"
    }
}
