import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.3

import "../Components/InputLabel"
import "../Components/HotkeyInput"

Page {
    Rectangle {
	width: window.width
	height: window.height - menu_bar.height
	Material.theme: Material.Dark
	Material.accent: Material.DeepPurple
	color: "#00EEEEEE"

	MouseArea {
	    anchors.fill: parent
	    onClicked: {
		settings.shortcuts_enabled = true;
	    }
	}

	ColumnLayout {
	    spacing: 2
	    anchors.fill: parent
	    
	    Column {
		CheckBox {
		    id: autoplay
		    checked: settings.autoplay_video
		    text: "Autoplay?"
		}
		
		InputLabel {
		    id: autoplay_delay

		    visible: autoplay.checked
		    leftPadding: autoplay.leftPadding

		    text_box_color: "#FFf48fb1"
		    text_box_height: autoplay.implicitIndicatorHeight
		    text_box_width: autoplay.implicitIndicatorWidth
		    
		    label_text: "Delay(s)?"
		    label_pad: autoplay.spacing
		}
		
		CheckBox {
		    id: transitions
		    checked: settings.transitions
		    text: "Enable transitions?"
		}
		
		CheckBox {
		    id: randomize_transitions
		    checked: settings.randomize_transitions && settings.transitions
		    text: "Randomize Transitions?"
		    visible: transitions.checked
		}
		
		CheckBox {
		    id: randomize_videos
		    checked: settings.randomize_videos
		    text: "Randomize Videos?"
		}

		HotkeyInput {
		    id: hotkey_play_video
		    
		    hotkey_id: "GlobalVideoPlay"
		    // leftPadding: autoplay.leftPadding
		    hotkey_callback: scripts.globalHotkeyCallbackLookup("VideoPlay")

		    input_width: 100
		    input_height: autoplay.implicitIndicatorHeight
		    input_border_color: 'black'
		    input_border_width: 1

		    label_text: "Hotkey for play/pause video?"
		    label_pad: autoplay.spacing
		}
	    }
	}
    }
}
