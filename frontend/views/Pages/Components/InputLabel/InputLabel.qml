import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

Row {
    property string text_box_color;
    property int text_box_width;
    property int text_box_height;

    property string label_text;
    property int label_pad;

    Rectangle {
	color: text_box_color
	width: text_box_width
	height: text_box_height
	border.color: 'black'
	border.width: 1

	visible: parent.visible
	
	TextInput {
	    anchors.centerIn: parent
	    text: settings.autoplay_delay
	    validator: IntValidator { bottom: 0; top: 100 }
	}
    }

    Label {
	visible: parent.visible 
	text: "Delay(s)?"
	leftPadding: autoplay.spacing
    }
    
}
