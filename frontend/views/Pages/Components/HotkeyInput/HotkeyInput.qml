import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.3

RowLayout {
    property string hotkey_id;
    property var hotkey_callback;

    property int input_height;
    property int input_width;
    property string input_border_color;
    property int input_border_width;

    property string label_text;
    property int label_pad;

    Label {
	text: label_text
    }
    
    Rectangle {
	color: "white"
	Layout.minimumWidth: input_width
	Layout.minimumHeight: input_height
	border.color: input_border_color
	border.width: input_border_width
	
	Text {
	    id: hotkey_text_obj

	    Component.onCompleted: {
		scripts.delay(500, () => {
		    this.text = scripts.globalHotkeyTextLookup(hotkey_id);
		    console.log('text obj:', this.text);
		});
	    }
	}

	Item {
	    id: hotkey_item
	    
	    Keys.onShortcutOverride: {
		if (!event.text) return;

		const hotkey_text = scripts.validateHotKey(event.modifiers, event.key);
		const success = scripts.createGlobalHotkey(
		    event.key,
		    event.modifiers,
		    hotkey_text,
		    hotkey_id,
		    hotkey_callback
		);
		
		if (success) hotkey_text_obj.text = hotkey_text;
	    }
	}

	MouseArea {
	    anchors.fill: parent
	    onClicked: {
		hotkey_item.focus = true;
		settings.shortcuts_enabled = false;
	    }

	    Keys.forwardTo: [hotkey_item]
	}
    }

    Button {
	Layout.minimumHeight: input_height
	text: "Ok"

	onClicked: {
	    hotkey_item.focus = false;
	    settings.shortcuts_enabled = true;
	}
    }
}
