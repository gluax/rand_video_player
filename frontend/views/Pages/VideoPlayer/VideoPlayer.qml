import QtMultimedia 5.15
import QtQuick 2.9
import QtQuick.Controls 2.5

Page {
    function load() {
	settings.videos.shuffle();
	settings.video_index = 0;
	settings.video_or_transition = true;
	video.source = `${settings.openProjectPath}/videos/${settings.videos[0]}`;
    }

    function set_source(source) {
	video.source = source;
    }
    
    function video_play() {
	if (settings.openProject === '') {
	    scripts.openError("No Open Project", () => {});
	    return;
	}
	
	if (video.status === MediaPlayer.PlayingState) {
	    video.pause();
	    settings.videos.shuffle();
	    settings.video_index = 0;

	    video.source = `${settings.openProjectPath}/videos/${settings.videos[0]}`;
	    return;
	}

	video.play();
    }

    Video {
	id: video
	width: parent.width
	height: parent.height - menu_bar.height

	Keys.onSpacePressed: video.playbackState === MediaPlayer.PlayingState ? video.pause() : video.play();
	Keys.onLeftPressed: video.seek(video.position - 5000);
	Keys.onRightPressed: video.seek(video.position + 5000);

	volume: volumeSlider.volume

	Component.onCompleted: {
	    if (settings.openProjectPath) video.source = `${settings.openProjectPath}/videos/${settings.videos[0]}`;
	    fileio.fileExists(video.source);
	}

	MouseArea {
	    anchors.fill: parent
	    onClicked: {
		if (settings.openProject === '') {
		    scripts.openError("No Open Project.", () => {});
		    return;
		}

		console.log(settings.videos)
		
		video.playbackState === MediaPlayer.PlayingState ? video.pause() : video.play();
	    }
	}     

	onStatusChanged: {
	    if (video.status === MediaPlayer.EndOfMedia) {
		if (settings.video_index === settings.videos.length - 1) return;
		if (settings.transition) settings.video_or_transition = !settings.video_or_transition;
		if (!settings.transitions && !(settings.video_index === settings.videos.length - 1)) settings.video_index += 1;

		if (settings.video_or_transition) {
		    video.source = settings.openProjectPath + "/videos/" + settings.videos[settings.video_index];
		    video.pause();
		    video.play();
		    return;
		}
		
		if (settings.transitions && !settings.video_or_transition) {
		    video.source = settings.openProjectPath + "/transitions/" + settings.transitions[settings.video_index];
		    if (!(settings.video_index === settings.videos.length - 1)) settings.video_index += 1;
		    video.pause();
		    video.play();
		    return;
		}
	    }
	}
    }

    Slider {
	id: volumeSlider

	property real volume: QtMultimedia.convertVolume(volumeSlider.value,
							 QtMultimedia.LogarithimicVolumeScale,
							 QtMultimedia.LinearVolumeScale)
    }
}
