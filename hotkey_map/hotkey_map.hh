#ifndef HotkeyMap_HH_
#define HotkeyMap_HH_

#include <QtCore/QDebug>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtQml/QJSValue>

class HotkeyMap : public QObject {
  Q_OBJECT

public slots:
  Q_INVOKABLE QJSValue get_hotkey_object(const QString& binding);
  Q_INVOKABLE bool exists_hotkey_object(const QString& binding);
  Q_INVOKABLE bool insert_hotkey_object(const QString& binding, QJSValue hotkey);
  Q_INVOKABLE bool remove_hotkey_object(const QString& binding);
  Q_INVOKABLE QList<QString> hotkey_object_keys();

  Q_INVOKABLE QString get_hotkey_text(const QString& binding);
  Q_INVOKABLE bool exists_hotkey_text(const QString& binding);
  Q_INVOKABLE bool insert_hotkey_text(const QString& binding, QString hotkey);
  Q_INVOKABLE bool remove_hotkey_text(const QString& binding);
  Q_INVOKABLE QList<QString> hotkey_text_keys();
  
public:
  HotkeyMap();
  ~HotkeyMap();

private:
  QMap<QString, QJSValue> _hotkey_object_map;
  QMap<QString, QString> _hotkey_text_map;
};

#endif  // HotkeyMap_HH_
