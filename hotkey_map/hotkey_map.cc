#include "hotkey_map.hh"

HotkeyMap::HotkeyMap() {}
HotkeyMap::~HotkeyMap() {}

QJSValue HotkeyMap::get_hotkey_object(const QString& binding) {
  return _hotkey_object_map.value(binding);
}

bool HotkeyMap::exists_hotkey_object(const QString& binding) {
  return _hotkey_object_map.contains(binding);
}

bool HotkeyMap::insert_hotkey_object(const QString& binding, QJSValue hotkey) {
  _hotkey_object_map.insert(binding, hotkey);
  return _hotkey_object_map.contains(binding);
}

bool HotkeyMap::remove_hotkey_object(const QString& binding) {
  _hotkey_object_map.remove(binding);
  return _hotkey_object_map.contains(binding);
}

QList<QString> HotkeyMap::hotkey_object_keys() {
  return _hotkey_object_map.keys();
}

QString HotkeyMap::get_hotkey_text(const QString& binding) {
  return _hotkey_text_map.value(binding);
}

bool HotkeyMap::exists_hotkey_text(const QString& binding) {
  return _hotkey_text_map.contains(binding);
}

bool HotkeyMap::insert_hotkey_text(const QString& binding, QString hotkey) {
  _hotkey_text_map.insert(binding, hotkey);
  return _hotkey_text_map.contains(binding);
}

bool HotkeyMap::remove_hotkey_text(const QString& binding) {
  _hotkey_text_map.remove(binding);
  return _hotkey_text_map.contains(binding);
}

QList<QString> HotkeyMap::hotkey_text_keys() {
  return _hotkey_text_map.keys();
}
