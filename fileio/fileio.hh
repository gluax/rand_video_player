#ifndef FILEIO_HH_
#define FILEIO_HH_

#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>
#include <QtCore/QObject>
#include <QtCore/QTextStream>
#include <QtCore/QUrl>

class FileIO : public QObject {
  Q_OBJECT

 public slots:
  Q_INVOKABLE bool newDir(const QString& source);
  Q_INVOKABLE bool fileExists(const QString& source);
  Q_INVOKABLE bool folderExists(const QString& source);
  Q_INVOKABLE bool newFile(const QString& source, const QString& data);
  Q_INVOKABLE QStringList scanFolderFiles(const QString& source);

 public:
  FileIO();
  ~FileIO();
};

#endif  // FILEIO_HH_
